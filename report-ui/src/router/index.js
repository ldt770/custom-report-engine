import {createRouter, createWebHashHistory} from 'vue-router'

const routes = [
  { path: '/definition', component: () => import('../views/Definition') },
  { path: '/definition/edit/:id', component: () => import('../views/Definition') },
  {
    path: '/report',
    component: () => import('../views/List'),
    children: [
      {
        path: ':id',
        component: () => import('../views/Report'),
      }
    ]
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
