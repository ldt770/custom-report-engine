create table report.report_definition
(
    id int auto_increment comment '主键'
        primary key,
    report_name varchar(64) not null comment '报表名',
    sql_template text not null comment 'SQL模板',
    configure text not null comment '报表配置',
    create_time timestamp default CURRENT_TIMESTAMP not null comment '创建时间',
    update_time timestamp default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP comment '更新时间'
);

