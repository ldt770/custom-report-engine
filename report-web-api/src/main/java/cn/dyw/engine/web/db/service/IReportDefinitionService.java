package cn.dyw.engine.web.db.service;

import cn.dyw.engine.web.db.entity.ReportDefinition;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author dyw770
 * @since 2021-08-17
 */
public interface IReportDefinitionService extends IService<ReportDefinition> {

}
