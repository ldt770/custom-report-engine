package cn.dyw.engine.web.configuration;

import cn.dyw.engine.core.exception.EngineExecException;
import cn.dyw.engine.web.api.rs.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全局异常处理
 *
 * @author dyw770
 * @since 2021-08-11
 */
@Slf4j
@RestControllerAdvice
public class GlobalDefaultExceptionHandler {

    /**
     * 参数类型不匹配
     *
     * @param e
     * @return
     */
    @ExceptionHandler(EngineExecException.class)
    public Result<String> engineExecExceptionHandler(EngineExecException e) {
        // TODO 后期优化错误信息输出
        return Result.createFail(Result.EXEC_ERROR, e.getMessage());
    }
}
