package cn.dyw.engine.web.db.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author dyw770
 * @since 2021-08-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ReportDefinition implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 报表名字
     */
    private String reportName;

    /**
     * SQL模板
     */
    private String sqlTemplate;

    /**
     * 报表配置
     */
    private String configure;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;


}
