package cn.dyw.engine.web.api.model;

import cn.dyw.engine.core.context.DynamicFilterParameter;
import cn.dyw.engine.core.context.HeaderBind;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 报表的配置
 *
 * @author dyw770
 * @since 2021-08-17
 */
public class ReportConfigure {

    /**
     * 参数
     */
    @Getter
    private final List<DynamicFilterParameter> parameters = new ArrayList<>(16);

    /**
     * 映射字段
     */
    @Getter
    @Setter
    private Map<String, HeaderBind> headerNames = new HashMap<>();

}
