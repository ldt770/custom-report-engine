package cn.dyw.engine.web.api.rs;

import cn.dyw.engine.web.api.model.ReportConfigure;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 报表定义
 * @author dyw770
 * @since 2021-08-18
 */
@Data
public class MiniReportRs {

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 配置
     */
    private ReportConfigure configure;

    /**
     * 报表名字
     */
    private String reportName;
}
