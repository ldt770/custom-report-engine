package cn.dyw.engine.web.api.rq;

import cn.dyw.engine.web.api.model.ReportConfigure;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * 报表定义请求
 *
 * @author dyw770
 * @since 2021-08-06
 */
@Data
public class ReportSaveRq {

    /**
     * sql模板
     */
    @Getter
    @Setter
    private String sqlTemplate;

    /**
     * 报表配置
     */
    @Getter
    @Setter
    private ReportConfigure configure;

    /**
     * 报表名字
     */
    @Getter
    @Setter
    private String reportName;
}
