package cn.dyw.engine.web.db.mapper;

import cn.dyw.engine.web.db.entity.ReportDefinition;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author dyw770
 * @since 2021-08-17
 */
@Mapper
public interface ReportDefinitionMapper extends BaseMapper<ReportDefinition> {

}
