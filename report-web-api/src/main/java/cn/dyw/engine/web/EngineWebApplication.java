package cn.dyw.engine.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动应用
 *
 * @author dyw770
 * @since 2021-08-06
 */
@SpringBootApplication
public class EngineWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(EngineWebApplication.class, args);
    }
}
