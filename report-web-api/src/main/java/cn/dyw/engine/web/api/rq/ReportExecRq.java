package cn.dyw.engine.web.api.rq;

import cn.dyw.engine.core.context.DynamicFilterParameter;
import cn.dyw.engine.core.limit.ExecLimit;
import cn.dyw.engine.core.order.ExecSort;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * 报表执行请求
 *
 * @author dyw770
 * @since 2021-08-18
 */
@Data
public class ReportExecRq {

    /**
     * 参数
     */
    @Getter
    @Setter
    private List<DynamicFilterParameter> parameters = new ArrayList<>(16);


    @Getter
    @Setter
    private List<ExecSort> sorts = new ArrayList<>(16);

    @Getter
    @Setter
    private ExecLimit limit;
}
