package cn.dyw.engine.web.api.rs;

import lombok.Getter;
import lombok.Setter;

/**
 * 统一返回结果
 *
 * @param <T> 返回数据类型
 * @author dyw770
 * @since 2021-08-06
 */
public class Result<T> {

    public static final int SUCCESS = 200;

    public static final int LOGIN_FAILURE = 201;

    public static final int FORBIDDEN = 403;

    public static final int NOT_LOGIN = 401;

    public static final int VIOLATION_FAILURE = 400;
    /**
     * 客户端请求信息的先决条件错误
     **/
    public static final int PRECONDITION_FAILURE = 412;
    /**
     * 服务器无法根据客户端请求的内容特性完成请求
     **/
    public static final int NOT_ACCEPTABLE = 406;

    public static final int NOT_IMPLEMENTED = 501;

    /**
     * 执行报表错误
     */
    public static final int EXEC_ERROR = 800;

    @Getter
    @Setter
    private int code;

    @Getter
    @Setter
    private String msg;

    @Getter
    @Setter
    private boolean success;

    @Getter
    @Setter
    private T data;

    public Result(int code, String msg, boolean success, T data) {
        this.code = code;
        this.msg = msg;
        this.success = success;
        this.data = data;
    }

    public Result(int code, String msg, boolean success) {
        this.code = code;
        this.msg = msg;
        this.success = success;
    }

    /**
     * 快速返回一个成功操作
     *
     * @param msg 消息
     * @param data 数据
     * @param <T> 返回数据
     * @return 返回结果
     */
    public static <T> Result<T> createSuccess(String msg, T data) {
        return new Result<>(SUCCESS, msg, true, data);
    }


    /**
     * 快速返回一个成功操作
     *
     * @param msg 消息
     * @param <T> 返回数据
     * @return 返回结果
     */
    public static <T> Result<T> createSuccess(String msg) {
        return new Result<>(SUCCESS, msg, true);
    }

    /**
     * 快速返回一个失败操作
     *
     * @param msg 消息
     * @param data 数据
     * @param <T> 返回数据
     * @return 返回结果
     */
    public static <T> Result<T> createFail(int code, String msg, T data) {
        return new Result<>(code, msg, false, data);
    }

    /**
     * 快速返回一个失败操作
     *
     * @param msg 消息
     * @param <T> 返回数据
     * @return 返回结果
     */
    public static <T> Result<T> createFail(int code, String msg) {
        return new Result<>(code, msg, false);
    }
}
