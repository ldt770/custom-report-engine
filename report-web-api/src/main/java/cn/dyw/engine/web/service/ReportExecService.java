package cn.dyw.engine.web.service;

import cn.dyw.engine.core.context.ReportStandardContext;
import cn.dyw.engine.core.exception.EngineExecException;
import cn.dyw.engine.core.exec.IReportExecEngine;
import cn.dyw.engine.core.exec.ReportResult;
import cn.dyw.engine.web.api.rq.ReportDefinitionRq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;

/**
 * 报表执行服务
 *
 * @author dyw770
 * @since 2021-08-06
 */
@Service
public class ReportExecService {

    @Autowired
    private IReportExecEngine execEngine;

    @Autowired
    private DataSource dataSource;

    public ReportResult reportExec(ReportDefinitionRq rq) throws EngineExecException {
        ReportStandardContext context =
                new ReportStandardContext(rq.getSqlTemplate(), rq.getHeaderNames());
        context.addParameter(rq.getParameters());
        context.addSorts(rq.getSorts());
        context.setDataSource(dataSource);
        context.setLimit(rq.getLimit());
        return execEngine.exec(context);
    }
}
