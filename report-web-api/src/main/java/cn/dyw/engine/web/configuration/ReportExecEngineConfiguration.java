package cn.dyw.engine.web.configuration;

import cn.dyw.engine.core.exec.DefaultReportExecEngine;
import cn.dyw.engine.core.exec.IReportExecEngine;
import cn.dyw.engine.core.template.DefaultTemplateEngine;
import cn.dyw.engine.core.template.ITemplateEngine;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * sql引擎配置
 *
 * @author dyw770
 * @since 2021-08-06
 */
@Configuration
public class ReportExecEngineConfiguration {

    @Bean
    public IReportExecEngine execEngine(ITemplateEngine templateEngine) {
        return new DefaultReportExecEngine(templateEngine);
    }

    @Bean
    public ITemplateEngine templateEngine() {
        return new DefaultTemplateEngine();
    }
}
