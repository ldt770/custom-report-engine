package cn.dyw.engine.web.api;

import cn.dyw.engine.core.exception.EngineExecException;
import cn.dyw.engine.core.exec.ReportResult;
import cn.dyw.engine.web.api.rq.ReportExecRq;
import cn.dyw.engine.web.api.rs.MiniReportRs;
import cn.dyw.engine.web.api.rs.Result;
import cn.dyw.engine.web.api.rs.SimpleReportRs;
import cn.dyw.engine.web.db.service.impl.ReportDefinitionServiceImpl;
import cn.dyw.engine.web.service.ReportExecService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 报表外部执行api
 *
 * @author dyw770
 * @since 2021-08-18
 */
@RestController
@RequestMapping("/report")
public class ReportApi {

    @Autowired
    private ReportExecService reportExecService;

    @Autowired
    private ReportDefinitionServiceImpl reportDefinitionServiceImpl;

    /**
     * 获取自定义报表
     *
     * @param id 报表ID
     * @return 报表
     *
     * @throws EngineExecException 获取的异常
     */
    @GetMapping("/{id}")
    public Result<MiniReportRs> one(@PathVariable("id") int id) throws EngineExecException {
        MiniReportRs rs = reportDefinitionServiceImpl.miniReportRs(id);
        return Result.createSuccess("查询成功", rs);
    }


    /**
     * 执行已定义报表
     *
     * @param id 报表ID
     * @param rq 参数
     * @return 报表
     *
     * @throws EngineExecException 获取的异常
     */
    @PostMapping("/{id}")
    public Result<ReportResult> exec(@PathVariable("id") int id, @RequestBody ReportExecRq rq) throws EngineExecException {
        ReportResult rs = reportDefinitionServiceImpl.exec(id, rq);
        return Result.createSuccess("执行成功", rs);
    }

    /**
     * 获取自定义报表
     *
     * @return 报表
     *
     */
    @GetMapping("")
    public Result<List<SimpleReportRs>> list() {
        List<SimpleReportRs> rs = reportDefinitionServiceImpl.simpleList();
        return Result.createSuccess("查询成功", rs);
    }
}
