package cn.dyw.engine.web.api.rq;

import cn.dyw.engine.core.context.DynamicFilterParameter;
import cn.dyw.engine.core.context.HeaderBind;
import cn.dyw.engine.core.limit.ExecLimit;
import cn.dyw.engine.core.order.ExecSort;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 报表定义请求
 *
 * @author dyw770
 * @since 2021-08-06
 */
@Data
public class ReportDefinitionRq {

    /**
     * sql模板
     */
    @Getter
    @Setter
    private String sqlTemplate;

    /**
     * 参数
     */
    @Getter
    @Setter
    private List<DynamicFilterParameter> parameters = new ArrayList<>(16);

    /**
     * 映射字段
     */
    @Getter
    @Setter
    private Map<String, HeaderBind> headerNames = new HashMap<>();


    @Getter
    @Setter
    private List<ExecSort> sorts = new ArrayList<>(16);

    @Getter
    @Setter
    private ExecLimit limit;
}
