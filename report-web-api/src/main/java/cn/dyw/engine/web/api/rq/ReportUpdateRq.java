package cn.dyw.engine.web.api.rq;


import lombok.Getter;
import lombok.Setter;

/**
 * 报表定义请求
 *
 * @author dyw770
 * @since 2021-08-06
 */
public class ReportUpdateRq extends ReportSaveRq {

    /**
     * id
     */
    @Getter
    @Setter
    private int id;
}
