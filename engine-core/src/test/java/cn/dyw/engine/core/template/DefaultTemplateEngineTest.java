package cn.dyw.engine.core.template;

import cn.dyw.engine.core.context.DynamicFilterParameter;
import cn.dyw.engine.core.context.ReportStandardContext;
import cn.dyw.engine.core.exception.EngineRenderException;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;

public class DefaultTemplateEngineTest {

    public DefaultTemplateEngine defaultTemplateEngine = new DefaultTemplateEngine();

    @Test
    public void processCommonParameter() throws EngineRenderException {
        String sqlTemplate = "select * from order_data where order_id = ${reverse(orderId)}";
        ReportStandardContext context = new ReportStandardContext(sqlTemplate, new HashMap<>());

        DynamicFilterParameter parameter = new DynamicFilterParameter();
        parameter.setParameterName("orderId");
        parameter.setParameterValue("12");
        context.addParameter(parameter);

        String sql = defaultTemplateEngine.process(context);

        Assert.assertEquals("select * from order_data where order_id = 21", sql);
    }

}