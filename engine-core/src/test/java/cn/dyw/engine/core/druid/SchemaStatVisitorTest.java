package cn.dyw.engine.core.druid;

import com.alibaba.druid.DbType;
import com.alibaba.druid.sql.SQLUtils;
import com.alibaba.druid.sql.ast.SQLLimit;
import com.alibaba.druid.sql.ast.SQLOrderBy;
import com.alibaba.druid.sql.ast.SQLStatement;
import com.alibaba.druid.sql.ast.statement.SQLSelectQueryBlock;
import com.alibaba.druid.sql.ast.statement.SQLSelectStatement;
import com.alibaba.druid.sql.visitor.SchemaStatVisitor;
import org.junit.Test;

/**
 * 测试
 * @author dyw770
 * @since 2021-09-01
 */
public class SchemaStatVisitorTest {

    @Test
    public void testSchemaStatVisitor() {

        String sql = "select name, age from t_user where id = 1 limit 10";

        SQLStatement stmt = SQLUtils.parseSingleStatement(sql, DbType.mysql);

        SchemaStatVisitor statVisitor = SQLUtils.createSchemaStatVisitor(DbType.mysql);
        stmt.accept(statVisitor);

        System.out.println(statVisitor.getColumns()); // [t_user.name, t_user.age, t_user.id]
        System.out.println(statVisitor.getTables()); // {t_user=Select}
        System.out.println(statVisitor.getConditions()); // [t_user.id = 1]

    }

    @Test
    public void testSQLSelectLimitStatement() {
        String sql = "select name, age from t_user where id = 1 limit 10";
        SQLSelectStatement stmt = (SQLSelectStatement) SQLUtils.parseSingleStatement(sql, DbType.mysql);
        SQLLimit limit = stmt.getSelect().getQueryBlock().getLimit();
        System.out.println(limit);
        stmt.getSelect().getQueryBlock().setLimit(null);
        System.out.println(stmt);
    }

    @Test
    public void testSQLSelectOrderStatement() {
        String sql = "select name, age from t_user where id = 1 order by age limit 10";
        SQLSelectStatement stmt = (SQLSelectStatement) SQLUtils.parseSingleStatement(sql, DbType.mysql);
        SQLOrderBy orderBy = new SQLOrderBy();
        stmt.getSelect().getQueryBlock().getOrderBy().addItem(SQLUtils.toOrderByItem("name", DbType.mysql));
        System.out.println(stmt);
    }

    @Test
    public void testSQLSelectStatement() {
        String sql = "select name, age from t_user where id = 1 limit 10";
        SQLSelectStatement stmt = (SQLSelectStatement) SQLUtils.parseSingleStatement(sql, DbType.mysql);
        // SQL转换
        // System.out.println(SQLUtils.toSQLString(stmt.getSelect().getQueryBlock(), DbType.sqlserver));
        SQLSelectQueryBlock sqb = new SQLSelectQueryBlock(DbType.mysql);

        sqb.setFrom(stmt.getSelect(), "tmp_sql");
        SQLOrderBy orderBy = new SQLOrderBy();
        orderBy.addItem(SQLUtils.toOrderByItem("name", DbType.mysql));
        sqb.setOrderBy(orderBy);
        sqb.addSelectItem(SQLUtils.toSelectItem("count(*)", DbType.mysql));
        System.out.println(sqb);
    }
}
