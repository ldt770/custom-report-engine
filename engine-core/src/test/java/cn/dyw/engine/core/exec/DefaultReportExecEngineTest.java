package cn.dyw.engine.core.exec;

import cn.dyw.engine.core.context.DynamicFilterParameter;
import cn.dyw.engine.core.context.ReportStandardContext;
import cn.dyw.engine.core.exception.EngineExecException;
import cn.dyw.engine.core.order.ExecSort;
import cn.dyw.engine.core.page.ExecPage;
import cn.dyw.engine.core.template.DefaultTemplateEngine;
import cn.dyw.engine.core.template.ITemplateEngine;
import com.alibaba.druid.pool.DruidDataSource;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.HashMap;


public class DefaultReportExecEngineTest {

    private final ITemplateEngine templateEngine = new DefaultTemplateEngine();

    private final IReportExecEngine execEngine = new DefaultReportExecEngine(templateEngine);

    private DataSource dataSource;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void init() {
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setUrl("jdbc:mysql://localhost:3306/report");
        dataSource.setUsername("root");
        dataSource.setPassword("20152046");
        this.dataSource = dataSource;
    }
    
    @Test
    public void execCommonSql() throws EngineExecException, JsonProcessingException, SQLException {
        String sqlTemplate = "select * from order_data where order_id = ${orderId}";
        ReportStandardContext context = new ReportStandardContext(sqlTemplate, new HashMap<>());
        context.addParameter(new DynamicFilterParameter("orderId", "1"));
        context.setConnection(dataSource.getConnection());
        ReportResult result = execEngine.exec(context);
        System.out.println(objectMapper.writeValueAsString(result));
    }

    @Test
    public void execOrderSql() throws EngineExecException, JsonProcessingException, SQLException {
        String sqlTemplate = "select * from order_data";
        ReportStandardContext context = new ReportStandardContext(sqlTemplate, new HashMap<>());
        context.addParameter(new DynamicFilterParameter("orderId", "1"));
        context.addSort(new ExecSort("order_id"));
        context.setConnection(dataSource.getConnection());
        ReportResult result = execEngine.exec(context);
        System.out.println(objectMapper.writeValueAsString(result));
    }

    @Test
    public void execPageSql() throws EngineExecException, JsonProcessingException, SQLException {
        String sqlTemplate = "select * from order_data";
        ReportStandardContext context = new ReportStandardContext(sqlTemplate, new HashMap<>());
        context.addParameter(new DynamicFilterParameter("orderId", "1"));
        context.addSort(new ExecSort("order_id"));
        context.setExecPage(new ExecPage(1, 10));
        context.setConnection(dataSource.getConnection());
        ReportResult result = execEngine.exec(context);
        System.out.println(objectMapper.writeValueAsString(result));
    }

    @Test
    public void testDataSource() throws Exception {
        System.out.println(this.dataSource.getConnection().getMetaData().getDatabaseProductName());
    }
}