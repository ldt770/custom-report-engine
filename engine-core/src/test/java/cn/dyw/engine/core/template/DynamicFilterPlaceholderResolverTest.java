package cn.dyw.engine.core.template;

import cn.dyw.engine.core.context.DynamicFilterParameter;
import org.junit.Test;
import org.springframework.context.expression.MapAccessor;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.expression.*;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.ReflectiveMethodExecutor;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.util.StringUtils;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class DynamicFilterPlaceholderResolverTest {

    @Test
    public void testSpel() throws NoSuchMethodException {
        SpelExpressionParser parser = new SpelExpressionParser();
        StandardEvaluationContext evaluationContext =
                new StandardEvaluationContext();

        DynamicFilterParameter parameter = new DynamicFilterParameter();
        parameter.setParameterName("aa");
        evaluationContext.setVariable("cleanPath",
                StringUtils.class.getDeclaredMethod("cleanPath", String.class));
//        evaluationContext.setVariable("test", "asda");
        Map<String, Object> map = new HashMap<>();
        map.put("da", "da");
//        ((StandardEvaluationContext)evaluationContext).setRootObject(map);
        evaluationContext.addPropertyAccessor(new MapAccessor());
        evaluationContext.addMethodResolver(new MethodResolver() {
            @Override
            public MethodExecutor resolve(EvaluationContext context, Object targetObject, String name, List<TypeDescriptor> argumentTypes) throws AccessException {
                Object o = context.lookupVariable(name);
                return new ReflectiveMethodExecutor((Method) o);
            }
        });
        Expression order = parser.parseExpression("cleanPath(da)");
        System.out.println(order.getValue(evaluationContext, map, String.class));
    }

}