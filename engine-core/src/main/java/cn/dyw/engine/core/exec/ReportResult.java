package cn.dyw.engine.core.exec;

import cn.dyw.engine.core.context.DynamicFilterParameter;
import cn.dyw.engine.core.context.HeaderBind;
import cn.dyw.engine.core.context.ReportStandardContext;
import cn.dyw.engine.core.order.ExecSort;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 报表结果
 *
 * @author dyw770
 * @since 2021-08-03
 */
public class ReportResult {

    @Getter
    private List<String> schema;

    @Getter
    private List<Map<String, Object>> data;

    @Getter
    @JsonIgnore
    private ReportStandardContext context;

    public ReportResult(ReportStandardContext context) {
        this();
        this.context = context;
    }

    private ReportResult() {
        schema = new ArrayList<>(32);
        data = new ArrayList<>(128);
    }

    /**
     * 返回字段映射
     * @return 字段映射
     */
    public Map<String, HeaderBind> getHeaderNames() {
        return context.getHeaderNames();
    }

    /**
     * 返回排序方式
     * @return 排序方式
     */
    public List<ExecSort> getSorts() {
        return context.getSorts();
    }

    /**
     * 返回参数
     * @return 字段映射
     */
    public List<DynamicFilterParameter> getParameters() {
        return context.getParameters();
    }

    /**
     * 初始化报表字段
     * @param schema 字段列表
     */
    public void initSchema(List<String> schema) {
        this.schema.addAll(schema);
    }

    /**
     * 添加数据
     * @param data 数据
     */
    public void addData(Map<String, Object> data) {
        this.data.add(data);
    }


    /**
     * 添加数据
     * @param datas 数据
     */
    public void addData(List<Map<String, Object>> datas) {
        this.data.addAll(datas);
    }
}
