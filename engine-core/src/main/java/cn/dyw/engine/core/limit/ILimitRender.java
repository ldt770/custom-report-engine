package cn.dyw.engine.core.limit;

import cn.dyw.engine.core.context.ReportStandardContext;

public interface ILimitRender {

    /**
     * 渲染limit
     *
     * @param sql     SQL
     * @param context SQL上下文
     * @return 带排序的SQL
     */
    String limitRender(String sql, ReportStandardContext context);
}
