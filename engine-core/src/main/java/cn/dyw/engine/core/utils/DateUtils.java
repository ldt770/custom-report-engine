package cn.dyw.engine.core.utils;

/**
 * 日期工具类
 *
 * @author dyw770
 * @since 2021-08-05
 */
public class DateUtils {

    /**
     * 字符串翻转
     *
     * @param s 参数
     * @return 翻转后的字符串
     */
    public static String reverse(String s) {
        char[] chars = s.toCharArray();
        String reverse = "";
        for (int i = chars.length - 1; i >= 0; i--) {
            reverse += chars[i];
        }
        return reverse;
    }
}
