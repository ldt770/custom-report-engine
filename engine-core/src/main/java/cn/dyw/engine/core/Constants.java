package cn.dyw.engine.core;

import com.alibaba.druid.DbType;

/**
 * 常量
 *
 * @author dyw770
 * @since 2021-09-07
 */
public class Constants {

    public static String TMP_ORDER_SQL_AlIAS = "tmp_query";

    public static DbType DEFAULT_DB_TYPE = DbType.mysql;

    public static String TMP_SELECT_SQL_AlIAS = "tmp_select_table";
}
