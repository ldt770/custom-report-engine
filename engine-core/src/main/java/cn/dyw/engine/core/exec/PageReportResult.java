package cn.dyw.engine.core.exec;

import cn.dyw.engine.core.context.ReportStandardContext;
import cn.dyw.engine.core.page.ExecPage;

/**
 * 带分页的结果
 *
 * @author dyw770
 * @since 2021-11-02
 */
public class PageReportResult extends ReportResult {

    public PageReportResult(ReportStandardContext context) {
        super(context);
    }

    public ExecPage getPage() {
        return this.getContext().getExecPage();
    }
}
