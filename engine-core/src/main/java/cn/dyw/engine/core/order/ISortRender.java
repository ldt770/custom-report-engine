package cn.dyw.engine.core.order;

import cn.dyw.engine.core.context.ReportStandardContext;

import java.util.HashMap;
import java.util.Map;

/**
 * 排序渲染器
 *
 * @author dyw770
 * @since 2021-08-09
 */
public interface ISortRender {

    /**
     * 渲染排序
     *
     * @param sql     SQL
     * @param context SQL上下文
     * @return 带排序的SQL
     */
    String sortRender(String sql, ReportStandardContext context);

}
