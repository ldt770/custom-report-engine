package cn.dyw.engine.core.page;

import cn.dyw.engine.core.context.ReportStandardContext;
import cn.dyw.engine.core.exception.EngineRenderException;

/**
 * 排序渲染器
 *
 * @author dyw770
 * @since 2021-08-09
 */
public interface IPageRender {

    /**
     * 渲染排序
     *
     * @param sql     SQL
     * @param context SQL上下文
     * @return 带排序的SQL
     */
    String pageRender(String sql, ReportStandardContext context) throws EngineRenderException;

}
