package cn.dyw.engine.core.page;

import lombok.Data;

/**
 * 分页参数
 *
 * @author dyw770
 * @since 2021-10-25
 */
@Data
public class ExecPage {

    private int page;

    private int size;

    private long count;

    public ExecPage(int page, int size) {
        this.page = page;
        this.size = size;
    }

    public ExecPage() {
    }
}
