package cn.dyw.engine.core.exception;

/**
 * 参数错误异常
 *
 * @author dyw770
 * @since 2021-08-03
 */
public class EngineFilterException extends EngineExecException {

    public EngineFilterException(String message, Throwable cause) {
        super(message, cause);
    }
}
