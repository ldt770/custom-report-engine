package cn.dyw.engine.core.template;

import cn.dyw.engine.core.context.ReportStandardContext;
import cn.dyw.engine.core.exception.EngineRenderException;

/**
 * sql渲染引擎
 *
 * @author dyw770
 * @since 2021-08-02
 */
public interface ITemplateEngine {

    /**
     * 渲染SQL
     * @param context 上下文
     * @return SQL渲染结果
     */
    String process(ReportStandardContext context) throws EngineRenderException;
}
