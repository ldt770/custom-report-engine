package cn.dyw.engine.core.context;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * SQL模板渲染需要的动态参数
 *
 * @author dyw770
 * @since 2021-08-03
 */
public class DynamicFilterParameter {

    @Getter
    @Setter
    private String parameterName;

    @Getter
    @Setter
    private String parameterValue;

    public DynamicFilterParameter() {
    }

    public DynamicFilterParameter(String parameterName, String parameterValue) {
        this.parameterName = parameterName;
        this.parameterValue = parameterValue;
    }


}
