package cn.dyw.engine.core.context;

import cn.dyw.engine.core.exec.ReportResult;
import cn.dyw.engine.core.limit.ExecLimit;
import cn.dyw.engine.core.order.ExecSort;
import cn.dyw.engine.core.page.ExecPage;
import com.alibaba.druid.DbType;
import lombok.Getter;
import lombok.Setter;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 报表上下文
 *
 * @author dyw770
 * @since 2021-08-02
 */
public class ReportStandardContext {

    /**
     * sql模板
     */
    @Getter
    @Setter
    private String sqlTemplate;

    /**
     * 渲染后的SQL
     */
    @Getter
    @Setter
    private String execSql;

    /**
     * 映射字段
     */
    @Getter
    @Setter
    private Map<String, HeaderBind> headerNames;

    /**
     * 参数
     */
    @Getter
    private final List<DynamicFilterParameter> parameters = new ArrayList<>(16);

    /**
     * 排序字段
     */
    @Getter
    private final List<ExecSort> sorts = new ArrayList<>(16);

    /**
     * limit 设置
     */
    @Getter
    @Setter
    private ExecLimit limit;

    /**
     * 参数设置
     */
    @Getter
    @Setter
    private ExecPage execPage;

    /**
     * 数据源
     */
    @Getter
    @Setter
    private Connection connection;

    /**
     * 数据源类型
     */
    @Getter
    @Setter
    private DbType dbType;

    /**
     * 结果
     */
    @Getter
    @Setter
    private ReportResult result;

    /**
     * 是否需要分页
     */
    @Getter
    @Setter
    private boolean needPage = true;


    public ReportStandardContext(String sqlTemplate, Map<String, HeaderBind> headerNames) {
        this.sqlTemplate = sqlTemplate;
        this.headerNames = headerNames;
    }

    public void addParameter(DynamicFilterParameter parameter) {
        parameters.add(parameter);
    }

    public void addParameter(List<DynamicFilterParameter> parameters) {
        this.parameters.addAll(parameters);
    }

    public void removeParameter(DynamicFilterParameter parameter) {
        this.parameters.remove(parameter);
    }

    public void clearParameter() {
        this.parameters.clear();
    }


    public void addSort(ExecSort sort) {
        this.sorts.add(sort);
    }

    public void addSorts(List<ExecSort> sorts) {
        this.sorts.addAll(sorts);
    }

    public void removeSort(ExecSort sort) {
        this.sorts.remove(sort);
    }

    public void clearSort() {
        this.sorts.clear();
    }
}
