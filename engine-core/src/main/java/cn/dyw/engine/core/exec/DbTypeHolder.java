package cn.dyw.engine.core.exec;

import cn.dyw.engine.core.Constants;
import com.alibaba.druid.DbType;

/**
 * dbtype
 *
 * @author dyw770
 * @since 2021-09-07
 */
public class DbTypeHolder {

    private static final ThreadLocal<DbType> threadLocal = ThreadLocal.withInitial(() -> Constants.DEFAULT_DB_TYPE);

    public static DbType get() {
        return threadLocal.get();
    }

    public static void remove() {
        threadLocal.remove();
    }

    public static void set(DbType dbType) {
        threadLocal.set(dbType);
    }
}
