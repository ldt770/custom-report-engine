package cn.dyw.engine.core.exec;

import cn.dyw.engine.core.context.ReportStandardContext;
import cn.dyw.engine.core.exception.EngineExecException;
import cn.dyw.engine.core.order.ISortRender;
import cn.dyw.engine.core.order.MysqlSortRender;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * 报表执行引擎
 *
 * @author dyw770
 * @since 2021-08-02
 */
public interface IReportExecEngine {

    /**
     * 执行报表
     * @param context 上下文
     */
    ReportResult exec(ReportStandardContext context)
            throws EngineExecException;

}
