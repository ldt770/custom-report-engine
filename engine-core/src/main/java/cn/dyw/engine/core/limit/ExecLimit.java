package cn.dyw.engine.core.limit;

import lombok.Data;

/**
 * limit
 *
 * @author dyw770
 * @since 2021-08-10
 */
@Data
public class ExecLimit {

    private int limit;

    private int page;
}
