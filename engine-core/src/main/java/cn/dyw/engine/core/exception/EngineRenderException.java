package cn.dyw.engine.core.exception;

/**
 * 参数渲染异常
 *
 * @author dyw770
 * @since 2021-08-03
 */
public class EngineRenderException extends EngineExecException {

    public EngineRenderException(String message, Throwable cause) {
        super(message, cause);
    }
}
