package cn.dyw.engine.core;

/**
 * 启动测试类
 *
 * @author dyw770
 * @since 2021-08-02
 */
public class App {

    public static void main(String[] args) {
        System.out.println("app start");
    }
}
