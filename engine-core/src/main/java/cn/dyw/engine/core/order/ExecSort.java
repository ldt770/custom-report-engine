package cn.dyw.engine.core.order;

import lombok.Getter;
import lombok.Setter;

/**
 * 排序字段
 *
 * @author dyw770
 * @since 2021-08-09
 */
public class ExecSort {

    /**
     * 排序字段名
     */
    @Setter
    @Getter
    private String fieldName;

    /**
     * 排序类型
     * 默认为 desc
     */
    @Setter
    @Getter
    private String orderType = "desc";

    public ExecSort(String fieldName, String orderType) {
        this.fieldName = fieldName;
        this.orderType = orderType;
    }

    public ExecSort(String fieldName) {
        this.fieldName = fieldName;
    }

    public ExecSort() {
    }
}
