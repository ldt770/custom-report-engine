package cn.dyw.engine.core.limit;

import cn.dyw.engine.core.context.ReportStandardContext;
import org.springframework.util.ObjectUtils;

/**
 * mysql limit
 *
 * @author dyw770
 * @since 2021-08-10
 */
public class MysqlLimitRender implements ILimitRender {

    @Override
    public String limitRender(String sql, ReportStandardContext context) {

        ExecLimit limit = context.getLimit();
        // TODO 后续采用比较优雅的方式实现
        if (!ObjectUtils.isEmpty(limit) && limit.getLimit() > 0) {
            return sql + " limit " + limit.getLimit();
        }

        return sql;
    }
}
