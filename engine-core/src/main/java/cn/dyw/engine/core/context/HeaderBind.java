package cn.dyw.engine.core.context;

import lombok.Data;

/**
 * 报表头配置
 *
 * @author dyw770
 * @since 2021-08-05
 */
@Data
public class HeaderBind {

    /**
     * 真实字段名
     */
    private String headerField;

    /**
     * 映射字段名
     */
    private String headerName;
}
